﻿using Domain.Entities.SubmitQuizEntity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentApis.SubmitQuizFluent
{
    public class SubmitTestDetailConfiguration : IEntityTypeConfiguration<SubmitTestDetail>
    {
        public void Configure(EntityTypeBuilder<SubmitTestDetail> builder)
        {
            builder.HasKey(t => t.Id);
            builder.HasOne(x => x.Test).WithMany(x => x.SubmitTestDetail).HasForeignKey(x => x.TestId);
        }
    }
}
