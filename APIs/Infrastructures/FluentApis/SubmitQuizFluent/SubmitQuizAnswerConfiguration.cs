﻿using Domain.Entities.SubmitQuizEntity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentApis.SubmitQuizFluent
{
    public class SubmitQuizAnswerConfiguration : IEntityTypeConfiguration<SubmitQuizAnswer>
    {
        public void Configure(EntityTypeBuilder<SubmitQuizAnswer> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne(x=>x.Option).WithMany(x=>x.SubmitQuizAnswers).HasForeignKey(x=>x.OptionID);
            builder.HasOne(x => x.SubmitTestDetail).WithMany(x => x.Answers).HasForeignKey(x => x.SubmitTestDetailId);
        }
    }
}
