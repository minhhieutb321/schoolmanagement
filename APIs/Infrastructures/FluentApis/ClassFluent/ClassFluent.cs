﻿using Domain.Entities.ClassEntity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentApis.ClassFulent
{
    public class ClassFluent : IEntityTypeConfiguration<Class>
    {
        public void Configure(EntityTypeBuilder<Class> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.ClassName).IsUnique();
            builder.HasIndex(x=>x.ClassCode).IsUnique();
            builder.HasOne(x => x.TrainingProgram).WithMany(x => x.Classes).HasForeignKey(x => x.TrainingProgramId);
        }
    }
}
