﻿using Domain.Entities.ClassEntity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentApis.ClassFulent
{
    public class ClassUserConfiguration : IEntityTypeConfiguration<ClassUsers>
    {
        public void Configure(EntityTypeBuilder<ClassUsers> builder)
        {
            builder.HasKey(x => x.ClassUserID);
            builder.HasOne(x=>x.User).WithMany(x=>x.ClassUsers).HasForeignKey(x=>x.UserId);
            builder.HasOne(x => x.Class).WithMany(x => x.ClassUsers).HasForeignKey(x => x.ClassId);
        }
    }
}
