﻿using Domain.Entities.SyllabusEntity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentApis.SyllabusFulent
{
    public class SyllabusConfiguration : IEntityTypeConfiguration<Syllabus>
    {
        public void Configure(EntityTypeBuilder<Syllabus> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasIndex(x=>x.Name).IsUnique();
            builder.HasIndex(x=>x.Code).IsUnique();
            builder.HasOne(x=>x.Subject).WithMany(x=>x.Syllabuses).HasForeignKey(x=>x.SubjectId);
        }
    }
}
