﻿using Domain.Entities.SyllabusEntity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentApis.SyllabusFulent
{
    public class SyllabusUnitConfiguration : IEntityTypeConfiguration<SyllabusUnit>
    {
        public void Configure(EntityTypeBuilder<SyllabusUnit> builder)
        {
            builder.HasKey(x => x.SyllabusUnitId);
            builder.HasOne(x => x.Syllabus).WithMany(x => x.SyllabusUnits).HasForeignKey(x => x.SyllabusId);
            builder.HasOne(x => x.Unit).WithMany(x => x.SyllabusUnits).HasForeignKey(x => x.UnitId);
        }
    }
}
