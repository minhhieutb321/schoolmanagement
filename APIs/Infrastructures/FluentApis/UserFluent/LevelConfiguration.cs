﻿using Domain.Entities.UserEntity;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentApis.UserFluent
{
    public class LevelConfiguration : IEntityTypeConfiguration<Level>
    {
        public void Configure(EntityTypeBuilder<Level> builder)
        {
            builder.HasData(
                new Level { Id=1,Name=LevelEnums.Intern},
                new Level { Id = 2, Name = LevelEnums.Fresher },
                new Level { Id = 3, Name = LevelEnums.Offline_Fee_Fresher },
                new Level { Id = 4, Name = LevelEnums.Online_Fee_Fresher },
                new Level { Id = 5, Name = LevelEnums.Junior },
                new Level { Id = 6, Name = LevelEnums.Middle },
                new Level { Id = 7, Name = LevelEnums.Senior }
           );
        }
    }
}
