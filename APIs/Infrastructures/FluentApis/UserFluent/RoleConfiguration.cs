﻿using Domain.Entities.UserEntity;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentApis.UserFluent
{
    public class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.HasData(new Role
                {
                RoleId = 1,
                RoleName = nameof(RoleEnums.SuperAdmin),
                ClassPermission = nameof(PermissionEnum.FullAccess),
                SyllabusPermission = nameof(PermissionEnum.FullAccess),
                LearningPermission = nameof(PermissionEnum.FullAccess),
                UserPermission = nameof(PermissionEnum.FullAccess),
                },
                new Role
                {
                    RoleId = 2,
                    RoleName = nameof(RoleEnums.Admin),
                    ClassPermission = nameof(PermissionEnum.FullAccess),
                    SyllabusPermission = nameof(PermissionEnum.FullAccess),
                    LearningPermission = nameof(PermissionEnum.FullAccess),
                    UserPermission = nameof(PermissionEnum.FullAccess)
                },
                new Role
                {
                    RoleId = 3,
                    RoleName = nameof(RoleEnums.Trainer),
                    ClassPermission = nameof(PermissionEnum.FullAccess),
                    SyllabusPermission = nameof(PermissionEnum.FullAccess),
                    LearningPermission = nameof(PermissionEnum.FullAccess),
                    UserPermission = nameof(PermissionEnum.FullAccess)
                },
                new Role
                {
                    RoleId = 4,
                    RoleName = nameof(RoleEnums.Trainee),
                    ClassPermission = nameof(PermissionEnum.View),
                    SyllabusPermission = nameof(PermissionEnum.FullAccess),
                    LearningPermission = nameof(PermissionEnum.FullAccess),
                    UserPermission = nameof(PermissionEnum.FullAccess)
                }
            );
        }
    }
}
