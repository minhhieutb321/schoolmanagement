﻿using Domain.Entities.TestEntity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentApis.TestFluent
{
    public class TestQuestionConfiguration : IEntityTypeConfiguration<TestQuestion>
    {
        public void Configure(EntityTypeBuilder<TestQuestion> builder)
        {
            builder.HasKey(x => x.TestQuestionId);
            builder.HasOne(x => x.Test).WithMany(x=>x.TestQuestions).HasForeignKey(x=>x.TestQuestionId);
            builder.HasOne(x => x.Question).WithMany(x => x.TestQuestions).HasForeignKey(x => x.TestQuestionId);
        }
    }
}
