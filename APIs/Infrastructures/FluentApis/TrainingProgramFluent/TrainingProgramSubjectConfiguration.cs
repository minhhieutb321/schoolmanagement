﻿using Domain.Entities.TrainingProgramEntity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentApis.TrainingProgramFluent
{
    public class TrainingProgramSubjectConfiguration : IEntityTypeConfiguration<TrainingProgramSubject>
    {
        public void Configure(EntityTypeBuilder<TrainingProgramSubject> builder)
        {
            builder.HasOne(x => x.TrainingProgram).WithMany(x => x.TrainingProgramSubjects).HasForeignKey(x => x.TrainingProgramId);
            builder.HasOne(x => x.Subject).WithMany(x => x.TrainingProgramSubjects).HasForeignKey(x => x.SubjectId);
        }
    }
}
