﻿using Domain.Entities.AttendanceEntity;
using Domain.Entities.ClassEntity;
using Domain.Entities.ModuleEntity;
using Domain.Entities.QuestionEntity;
using Domain.Entities.SubmitQuizEntity;
using Domain.Entities.SyllabusEntity;
using Domain.Entities.TestEntity;
using Domain.Entities.TrainingProgramEntity;
using Domain.Entities.UnitEntity;
using Domain.Entities.UserEntity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures
{
    public class AppDbContext:DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }
        #region DBSet  
        public DbSet<Attendance> Attendances { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<ClassUsers> ClassUsers { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Option> Options { get; set; }
        public DbSet<Question> Question { get;set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<SubmitQuizAnswer> SubmitQuizAnswers { get; set; }
        public DbSet<SubmitTestDetail> SubmitTestDetails { get; set; }
        public DbSet<Syllabus> Syllabuses { get; set; }
        public DbSet<SyllabusUnit> SyllabusUnits { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<TestQuestion> TestQuestions { get; set; }  
        public DbSet<TrainingProgram> TrainingPrograms { get; set; }    
        public DbSet<TrainingProgramSubject> TrainingProgramSubjects { get; set; }  
        public DbSet<Unit> Units { get; set; }
        public DbSet<Level> Levels { get; set; }    
        public DbSet<User> Users { get;set; }
        #endregion
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
