﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enums
{
    public static class LevelEnums
    {
        public static readonly string Intern = "Intern";
        public static readonly string Fresher = "Fresher";
        public static readonly string Online_Fee_Fresher = "Online fee-fresher";
        public static readonly string Offline_Fee_Fresher = "Offline fee-fresher";
        public static readonly string Junior = "Junior";
        public static readonly string Middle = "Middle";
        public static readonly string Senior = "Senior";
        
    }
}
