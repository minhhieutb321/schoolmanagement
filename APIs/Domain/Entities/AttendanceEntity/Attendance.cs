﻿using Domain.Entities.ClassEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.AttendanceEntity
{
    public class Attendance:BaseEntity
    {
        public DateTime CheckDate { get; set; }
        public string Status { get; set; }
        public Guid ClassUserId { get; set; }
        public ClassUsers User { get; set; }
    }
}
