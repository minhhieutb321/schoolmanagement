﻿using Domain.Entities.ClassEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.TrainingProgramEntity
{
    public class TrainingProgram:BaseEntity
    {
        public string Name { get; set; }
        public string Status { get; set; }
        public ICollection<TrainingProgramSubject> TrainingProgramSubjects { get; set; }
        public ICollection<Class> Classes { get; set; }
    }
}
