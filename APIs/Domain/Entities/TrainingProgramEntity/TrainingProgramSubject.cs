﻿using Domain.Entities.ModuleEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.TrainingProgramEntity
{
    public class TrainingProgramSubject:BaseEntity
    {
        public Guid TrainingProgramId { get; set; }
        public  TrainingProgram TrainingProgram { get; set; }
        public Guid SubjectId { get; set; }
        public Subject Subject { get; set; }
        public string Status { get; set; }
    }
}
