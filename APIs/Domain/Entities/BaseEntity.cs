﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public DateTime CreatedDate { get; set; }
        public Guid? CreateById { get; set; }
        public string? CreateByName { get; set; }
        public DateTime? ModificationDate { get; set; }
        public Guid? ModifiById { get; set; }
        public string? ModifiedByName { get; set; }
        public DateTime? DeletionDate { get; set; }
        public string? DeleteBy { get; set; }
        public bool? IsDeleted { get; set; }=false;
    }
}
