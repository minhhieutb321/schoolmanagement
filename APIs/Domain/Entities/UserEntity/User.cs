﻿using Domain.Entities.ClassEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.UserEntity
{
    public class User : BaseEntity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        /// <summary>
        /// Role
        /// </summary>
        public int RoleId { get; set; }
        public Role Role { get; set; }
        public int LevelId { get; set; }
        public Level Level { get; set; }
        /// <summary>
        /// Login
        /// </summary>
        public DateTime? LoginAt { get; set; }
        public string? RefreshToken { get; set; }
        public DateTime? ExpireTokenTime { get; set; }
        /// <summary>
        /// ClassUser
        /// </summary>
        public ICollection<ClassUsers> ClassUsers { get; set; }
    }
}
