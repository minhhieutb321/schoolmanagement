﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.UserEntity
{
    public class Role
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string SyllabusPermission { get; set; }
        public string ClassPermission { get; set; }
        public string LearningPermission { get; set; }
        public string UserPermission { get; set; }
        public ICollection<User> Users { get; set; }
    }
}
