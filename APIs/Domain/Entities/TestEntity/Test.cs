﻿using Domain.Entities.SubmitQuizEntity;
using Domain.Entities.UnitEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.TestEntity
{
    public class Test:BaseEntity
    {
        public string Name { get; set; }
        public int Duration { get; set; }
        public string Description { get; set; }
        public Guid? UnitId { get; set; }
        public Unit? Unit { get; set; }
        public ICollection<TestQuestion> TestQuestions { get; set; }
        public ICollection<SubmitTestDetail>? SubmitTestDetail { get; set; }
    }
}
