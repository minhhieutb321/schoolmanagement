﻿using Domain.Entities.QuestionEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.TestEntity
{
    public class TestQuestion
    {
        public Guid TestQuestionId { get; set; }
        public Guid TestID { get; set; }
        public Test Test { get; set; }  
        public Guid QuestionID { get; set; }
        public Question Question { get; set; }

    }
}
