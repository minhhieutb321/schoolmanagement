﻿using Domain.Entities.ModuleEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.SyllabusEntity
{
    public class Syllabus:BaseEntity
    {
        public string Name { get; set; }
        public string CourseObjective { get; set; }
        public string TechRequirements { get; set; }
        public int Duration { get; set; }
        public string Code { get; set; }
        public string OutputStandard { get; set; }
        public Guid SubjectId { get; set; }
        public Subject Subject { get; set; }

        public ICollection<SyllabusUnit> SyllabusUnits { get; set; }
    }
}
