﻿using Domain.Entities.UnitEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.SyllabusEntity
{
    public class SyllabusUnit
    {
        public Guid SyllabusUnitId { get; set; }
        public Guid SyllabusId { get; set; }
        public Syllabus Syllabus { get; set; }
        public Guid UnitId { get; set; }
        public Unit Unit { get; set; }

        public int Session { get; set; }
    }
}
