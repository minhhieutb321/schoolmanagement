﻿using Domain.Entities.SyllabusEntity;
using Domain.Entities.TestEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.UnitEntity
{
    public class Unit:BaseEntity
    {
        public string Name { get; set; }
        public int Duration { get; set; }
        public string StandardCode { get; set; }
        public ICollection<SyllabusUnit> SyllabusUnits { get; set; }
        public ICollection<Test>? Tests { get; set; }
    }
}
