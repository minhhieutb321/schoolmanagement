﻿using Domain.Entities.SyllabusEntity;
using Domain.Entities.TrainingProgramEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ModuleEntity
{
    public class Subject:BaseEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public ICollection<TrainingProgramSubject> TrainingProgramSubjects { get; set;}
        public ICollection<Syllabus> Syllabuses { get; set;}
    }
}
