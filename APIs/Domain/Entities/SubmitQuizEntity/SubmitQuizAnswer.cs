﻿using Domain.Entities.QuestionEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.SubmitQuizEntity
{
    public class SubmitQuizAnswer:BaseEntity
    {
        public string Answer { get; set; }
        public bool IsCorrect { get; set; }
        public float Point { get; set; }
        public Guid OptionID { get; set; }
        public Option Option { get; set; }
        public Guid SubmitTestDetailId { get; set; }
        public SubmitTestDetail SubmitTestDetail { get; set; }
    }
}
