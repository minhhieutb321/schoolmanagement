﻿using Domain.Entities.TestEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.SubmitQuizEntity
{
    public class SubmitTestDetail:BaseEntity
    {
        public double? TotalScore { get; set; }
        public int? TotalTime { get; set; }
        public int? NumOfCorrectAnswer { get; set; }
        public int? NumOfInCorrectAnswer { get; set; }
        public string? Comment { get; set; }
        public Guid TestId { get; set; }
        public Test Test { get; set; } 
        public ICollection<SubmitQuizAnswer> Answers{ get; set; }
    }
}
                              