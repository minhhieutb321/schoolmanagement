﻿using Domain.Entities.TestEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.QuestionEntity
{
    public class Question:BaseEntity
    {
        public string QuestionContent { get; set; }
        public string? URL { get; set; }
        public string? Description { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public ICollection<Option> Options { get; set; }
        public ICollection<TestQuestion> TestQuestions { get; set; }
    }
}
