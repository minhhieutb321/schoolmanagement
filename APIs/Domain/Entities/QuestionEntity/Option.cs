﻿using Domain.Entities.SubmitQuizEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.QuestionEntity
{
    public class Option:BaseEntity
    {
        public string Content { get; set; }
        public bool IsCorrect { get; set; }
        public float Point { get; set; }
        public Guid QuestionId { get; set; }
        public Question Question { get; set; }
        public ICollection<SubmitQuizAnswer>? SubmitQuizAnswers { get; set; }
    }
}
