﻿using Domain.Entities.AttendanceEntity;
using Domain.Entities.UserEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Domain.Entities.ClassEntity
{
    public class ClassUsers
    {
        public Guid ClassUserID { get; set; }=Guid.NewGuid();
        public Guid ClassId { get; set; }
        public Class Class { get; set; }   
        public Guid UserId { get; set; }
        public User User { get; set; }
        public string RoleStatus { get; set; }
        public ICollection<Attendance> Attendances { get; set; }
    }
}
