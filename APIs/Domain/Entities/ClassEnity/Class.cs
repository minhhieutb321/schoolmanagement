﻿using Domain.Entities.TrainingProgramEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ClassEntity
{
    public class Class:BaseEntity
    {
        public string ClassName { get; set; }      
        public string ClassCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public ICollection<ClassUsers> ClassUsers { get; set; }  
        /// <summary>
        /// Training Program
        /// </summary>
        public Guid TrainingProgramId { get; set; }
        public TrainingProgram TrainingProgram { get; set; }
    }
}
