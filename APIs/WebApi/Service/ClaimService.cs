﻿using Application.Interfaces;
using System.Security.Claims;

namespace WebApi.Service
{
    public class ClaimService : IClaimService
    {
        public ClaimService(IHttpContextAccessor httpContextAccessor)
        {
            // todo implementation to get the current userId
            var Id = httpContextAccessor.HttpContext?.User?.FindFirstValue("userId");
            GetCurrentUserId = string.IsNullOrEmpty(Id) ? Guid.Empty : Guid.Parse(Id);
            // todo implementation to get the current userName
            var name = httpContextAccessor.HttpContext?.User?.FindFirstValue("userName");
            GetCurrentName = string.IsNullOrEmpty(name) ? string.Empty : name;
        }
        public Guid GetCurrentUserId { get; }

        public string GetCurrentName { get; }
    }
}
