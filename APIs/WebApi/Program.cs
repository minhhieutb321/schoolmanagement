using Application.Commons;
using Infrastructures;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using WebApi;
using WebApi.Middlewares;
using WebApi.Middlewares.GlobalExceptionHandling.Utility;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

// parse the configuration in appsettings
var configuration = builder.Configuration.Get<AppConfiguration>();

builder.Services.AddInfrastructuresService(configuration!.DatabaseConnection);
builder.Services.AddWebAPIService();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseMiddleware(typeof(GlobalErrorHandlingMiddleware)); 
app.UseMiddleware<PerformanceMiddleware>();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
