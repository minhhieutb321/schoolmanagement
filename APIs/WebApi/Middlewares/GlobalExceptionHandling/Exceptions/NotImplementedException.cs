﻿namespace WebApi.Middlewares.GlobalExceptionHandling.Exceptions
{
    public class NotImplementedException : Exception
    {
        public NotImplementedException(string? message) : base(message)
        {
        }
    }
}
