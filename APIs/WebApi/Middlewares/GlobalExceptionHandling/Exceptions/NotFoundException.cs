﻿namespace WebApi.Middlewares.GlobalExceptionHandling.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string? message) : base(message)
        {
        }
    }
}
